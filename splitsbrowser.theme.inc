<?php

/**
 * @file
 * SplitsBrowser theming functionality.
 */

/**
 * Prepares variables for the SplitsBrowser application template.
 *
 * Default template: splitsbrowser-app.html.twig.
 *
 * @param $vars
 *   An associative array containing:
 *   - X: X
 */
function template_preprocess_splitsbrowser_app(&$vars) {
  $vars['#attached']['library'][] = 'splitsbrowser/integration';
}
