<?php

namespace Drupal\splitsbrowser\Controller;

use Drupal\Core\Controller\ControllerBase;

/**
 * Controller for the SplitsBrowser.
 */
class SplitsBrowserController extends ControllerBase {

  /**
   * Renders the SplitsBrowser.
   */
  public function page() {
    return [
      '#theme' => 'splitsbrowser_app',
    ];
  }

}
