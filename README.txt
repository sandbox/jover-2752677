CONTENTS OF THIS FILE
---------------------

 * Introduction
 * Requirements
 * Installation
 * Maintainers


INTRODUCTION
------------

Integrates the SplitsBrowser library into Drupal.


REQUIREMENTS
------------

This module requires the following libraries:

 * SplitsBrowser (http://www.splitsbrowser.org.uk/)


INSTALLATION
------------

 * Install as you would normally install a contributed Drupal module.
   See: https://www.drupal.org/documentation/install/modules-themes/modules-8
   for further information.


MAINTAINERS
-----------


