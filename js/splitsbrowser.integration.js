(function ($, Drupal) {

  'use strict';

  Drupal.behaviors.initSplitsBrowser = {
    attach: function (context, settings) {
      SplitsBrowser.loadEvent("/libraries/splitsbrowser/testdata/csv-1000-competitors", "#splitsbrowser-app");
    }
  };

})(jQuery, Drupal);